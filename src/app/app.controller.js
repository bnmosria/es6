class AppController {
    constructor(sliderCarouselService) {
        this.result = {};
        this.sliderCarouselService = sliderCarouselService;
        this.data = [];
    }

    $onInit() {
        this.isPizza = true;
        this.isCake = true;
        this.form = document.querySelector('.contact-form');

        this.sliderCarouselService.getData()
            .then(response => this.data = response.data);
    }

    onInputChange() {
        const data = this.formToJSON(this.form.elements);
    }

    onCheckboxChecked() {
        const data = this.formToJSON(this.form.elements);
    }

    formToJSON(elements) {
        this.result = [].reduce.call(elements, (data, element) => {
            data[element.name] = element.value;

            return data;
        }, {});
    }
}

AppController.$inject = ['sliderCarouselService'];

export default AppController;
