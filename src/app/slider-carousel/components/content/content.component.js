import template from './content.component.html';
import { ContentController as controller } from "./content.controller";

import './content.less';

export const ContentComponent = {
    template,
    controller,
    transclude: true,
};
