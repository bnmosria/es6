export class ContentController {
    constructor(
        $scope,
        $timeout,
        sliderCarouselService
    ){
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.sliderCarouselService = sliderCarouselService;
        this.data = [];

    }

    $onInit() {
        this.closeFooterContent();

        this.sliderCarouselService.getData()
            .then(response => {
                this._setData(response.data);

                this.$scope.$watch(
                    '$viewContentLoaded',
                    () => {
                        this.$timeout(() => {
                            this.sliderCarouselService.createSliderInstance();
                        });
                    }
                );
            });
    }

    $onDestroy() {
        this.sliderCarouselService.destroy();
    }

    onNextClicked() {
        this.sliderCarouselService.next();
    }

    onPrevClicked() {
        this.sliderCarouselService.prev();
    }

    onFooterButtonClicked() {
        this.showFooterContent = !this.showFooterContent;
    }

    closeFooterContent() {
        this.showFooterContent = false;
    }

    _setData(data) {
        this.data = data;
    }
}

ContentController.$inject = [
    '$scope',
    '$timeout',
    'sliderCarouselService'
];
