import Siema from 'siema/';

export class SliderCarouselService {
    constructor($http, $q, $timeout) {

        this.$http = $http;
        this.$q = $q;
        this.$timeout = $timeout;

        this.options = {
            selector: '.content__slider',
            duration: 300,
            easing: 'ease-out',
            perPage: 1,
            startIndex: 0,
            draggable: true,
            threshold: 300,
            loop: true,
        };
    }

    getData() {
        return this.$http.get('/data.json');
    }

    createSliderInstance() {
        this.siema = new Siema(this.options);
    }

    next() {
        this.siema.next();
    }

    prev() {
        this.siema.prev();
    }

    destroy() {
        this.siema.destroy();
    }
}

SliderCarouselService.$inject = ['$http', '$q', '$timeout'];
