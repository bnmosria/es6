import { ContentComponent } from "./components/content/content.component";
import { SliderCarouselService } from "./services/slider-carousel.service";
import { CaretIconModule } from "../svg-icons/caret-icon/caret-icon.module";
import { SliderArrowIconModule } from "../svg-icons/slider-arrow-icon/slider-arrow-icon.module";

export const SliderCarouselModule = angular
    .module('sliderCarousel', [
        CaretIconModule,
        SliderArrowIconModule
    ])
    .component('content', ContentComponent)
    .service('sliderCarouselService', SliderCarouselService)
    .name;
