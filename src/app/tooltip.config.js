export const TooltipConfig =  tooltipsConfProvider => {
    tooltipsConfProvider.configure({
        'size': 'large',
        'speed': 'fast',
        'closeButton': true,
        'tooltipTemplateUrlCache': true
    });
};

TooltipConfig.$inject = ['tooltipsConfProvider'];
