import ContentSliderComponent from './components/content-slider/content-slider.component';
import ContentSliderSlideComponent from './components/content-slider-slide/content-slider-slide.component';
import ContentSliderNavigationComponent from './components/content-slider-navigation/content-slider-navigation.component';

import TriggerSliderAnimation from './components/content-slider/trigger-slider.animation';
import ContentSliderConstants from './components/content-slider/content-slider.constants';

import SlideAnimationService from '../content-slider/components/content-slider/slide-animation.service';
import SlideDirectionService from './components/content-slider/slide-direction.service';
import MatcherService from './components/content-slider/matcher.service';

const CONTENT_SLIDER_SLIDE_SELECTOR = '.content-slider-slide';

let ContentSliderModule = angular
    .module('contentSlider', [
        'ngAnimate',
        'ngTouch'
    ])
    .animation(CONTENT_SLIDER_SLIDE_SELECTOR, TriggerSliderAnimation)
    .component('contentSlider', ContentSliderComponent)
    .component('contentSliderSlide', ContentSliderSlideComponent)
    .component('contentSliderNavigation', ContentSliderNavigationComponent)
    .constant('SLIDER_CONSTANTS', ContentSliderConstants)
    .service('slideAnimationService', SlideAnimationService)
    .service('slideDirectionService', SlideDirectionService)
    .service('matcherService', MatcherService)
    .name;

export default ContentSliderModule;