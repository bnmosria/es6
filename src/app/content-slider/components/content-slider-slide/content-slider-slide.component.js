import template from './content-slider-slide.component.html';
import controller from './content-slider-slide.controller';

import './content-slider-slide.less';

let component = {
    template,
    controller,
    transclude: true,
    require: {
        contentSliderController: '^contentSlider'
    }
};

export default component;