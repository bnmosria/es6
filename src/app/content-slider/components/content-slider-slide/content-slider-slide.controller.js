class ContentSliderSlideController {
    constructor($element) {
        this.$element = $element[0];
        this.isCurrentSlide = false;

        this.$onInit = this._onInit;
    }

    _onInit() {
        this.contentSliderController.register(this);
    }
}

ContentSliderSlideController.$inject = ['$element'];

export default ContentSliderSlideController;