import template from './content-slider.component.html';
import controller from './content-slider.controller';

import './content-slider.less';

let component = {
    template,
    controller,
    transclude: {
        slides: 'contentSliderSlide',
        navigation: '?contentSliderNavigation'
    },
    bindings: {
        title: '@?'
    }
};

export default component;