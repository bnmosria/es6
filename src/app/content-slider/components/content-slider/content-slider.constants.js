let constants = {
        SLIDE_TO_LEFT: 'toLeft',
        SLIDE_TO_RIGHT: 'toRight'
    };

export default constants;