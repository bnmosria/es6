class SlideDirectionService {
    constructor(SLIDER_CONSTANTS) {
        this.SLIDER_CONSTANTS = SLIDER_CONSTANTS;

        this.direction = this.SLIDER_CONSTANTS.SLIDE_TO_LEFT;
    }

    setDirection(direction) {
        this.direction = direction;
    }

    getDirection() {
        return this.direction;
    }
}

SlideDirectionService.$inject = ['SLIDER_CONSTANTS'];

export default SlideDirectionService;