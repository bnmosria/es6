class MatcherService {
    match(item, parameter) {
        return this.assertProfession(item.professionGroup, parameter.profession)
            && this.matchAgeGroup(item.ageGroup, parameter.age);
    }

    assertProfession(actualProfession, expectedProfession) {
        if (actualProfession === 'all') {
            return true;
        }

        return actualProfession.search(expectedProfession) !== -1;
    }

    matchAgeGroup(ageGroup, expectedAge) {
        if (ageGroup === 'all') {
            return true;
        }

        let [from, to] = ageGroup.split('_', 2);

        return (expectedAge >= this.toInt(from) && expectedAge <= this.toInt(to));
    }

    toInt(number) {
        return parseInt(number) || 0;
    }

}

export default MatcherService;