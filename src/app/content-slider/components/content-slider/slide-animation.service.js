const DEFAULT_SLIDE_DURATION = 500;
const CUBIC_BEZIER_EASY_IN_OUT = 'cubic-bezier(0.45, 0.03, 0.51, 0.95)';
const DEFAULT_ANIMATION_FILL_PROPERTY = 'forwards';
const SLIDE_IN = 'in';
const SLIDE_OUT = 'out';

class SlideAnimationService {
    constructor(SLIDER_CONSTANTS) {
        this.SLIDER_CONSTANTS = SLIDER_CONSTANTS;

        this.options = {
            duration: DEFAULT_SLIDE_DURATION,
            easing: CUBIC_BEZIER_EASY_IN_OUT,
            fill: DEFAULT_ANIMATION_FILL_PROPERTY
        };
    }

    slideIn(element, direction, callback) {
        let distance = this._getDistance(
            SLIDE_IN,
            element,
            this._getDirection(direction)
        );

        let transform = {transform: [
            'translateX(' + distance + 'px)',
            'translateX(0)'
        ]};

        this._transformTranslate(element, transform, callback);
    }

    slideOut(element, direction, callback) {
        let distance = this._getDistance(
            SLIDE_OUT,
            element,
            this._getDirection(direction)
        );

        let transform = {transform: [
            'translateX(0)',
            'translateX(' + distance + 'px)',
        ]};

        this._transformTranslate(element, transform, callback);
    }

    _getDistance(slideType, element, direction) {
        let parentRect = element.parentElement.getBoundingClientRect();

        if (slideType === SLIDE_IN) {
            return (direction === this.SLIDER_CONSTANTS.SLIDE_TO_LEFT)
                ? parentRect.width :
                -parentRect.width;
        }

        if (slideType === SLIDE_OUT) {
            return (direction === this.SLIDER_CONSTANTS.SLIDE_TO_LEFT)
                ? -parentRect.width
                : parentRect.width;
        }
    }

    _getDirection(direction) {
        if (angular.isUndefined(direction) || direction === null) {
            return this.SLIDER_CONSTANTS.SLIDE_TO_LEFT;
        }

        return direction;
    }

    _transformTranslate(element, transform, callback) {
        let animation = element.animate(transform, this.options);

        if (angular.isFunction(callback)) {
            animation.onfinish = () => {
                callback();
            };
        }
    }
}

SlideAnimationService.$inject = ['SLIDER_CONSTANTS'];

export default SlideAnimationService;