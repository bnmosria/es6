class ContentSliderController {
    constructor(
        slideDirectionService,
        SLIDER_CONSTANTS
    ) {
        this.slides = [];
        this.slideDirectionService = slideDirectionService;
        this.SLIDER_CONSTANTS = SLIDER_CONSTANTS;

        //Need to set the current index after slides are registered
        this.$postLink = () => this.setCurrentSlide(0);

        this.number = "4567,89";

        let [numerator, fraction] = this.number.split(',');

        this.numerator = parseInt(numerator).toLocaleString() ;
        this.fraction = fraction;
    }

    register(slide) {
        this.slides.push(slide);
    }

    nextSlide(){
        let currentIndex = (this.currentIndex < this.slides.length - 1)
            ? ++this.currentIndex
            : 0;

        this.slideDirectionService.setDirection(this.SLIDER_CONSTANTS.SLIDE_TO_LEFT);

        this.setCurrentSlide(currentIndex);
    }

    prevSlide() {
        let currentIndex = (this.currentIndex > 0)
            ? --this.currentIndex
            : this.slides.length - 1;

        this.slideDirectionService.setDirection(this.SLIDER_CONSTANTS.SLIDE_TO_RIGHT);

        this.setCurrentSlide(currentIndex);
    }

    setCurrentSlide(index) {
        this.currentIndex = index;

        angular.forEach(this.slides, (slide, key) => {
            slide.isCurrentSlide = index === key;
        });
    }

    slideTo(index) {
        // Need to set the direction before sliding,
        // then it should slide to the proper direction
        let direction = (index > this.currentIndex)
            ? this.SLIDER_CONSTANTS.SLIDE_TO_LEFT
            : this.SLIDER_CONSTANTS.SLIDE_TO_RIGHT;

        this.slideDirectionService.setDirection(direction);

        this.setCurrentSlide(index);
    }
}

ContentSliderController.$inject = [
    'slideDirectionService',
    'SLIDER_CONSTANTS'
];

export default ContentSliderController;