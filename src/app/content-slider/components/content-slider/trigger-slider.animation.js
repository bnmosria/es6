const DIRECTIVE_SELECTOR = 'ng-hide';

let triggerSliderAnimation = (
        slideDirectionService,
        slideAnimationService
    ) => {
        return {
            addClass: function (element, className, done) {
                if (className === DIRECTIVE_SELECTOR) {
                    slideAnimationService
                        .slideOut(
                            element[0],
                            slideDirectionService.getDirection(),
                            done
                        );
                }
                else {
                    done();
                }
            },
            removeClass: function (element, className, done) {
                if (className === DIRECTIVE_SELECTOR) {
                    slideAnimationService
                        .slideIn(
                            element[0],
                            slideDirectionService.getDirection(),
                            done
                        );
                }
                else {
                    done();
                }
            }
        };
    };

triggerSliderAnimation.$inject = [
    'slideDirectionService',
    'slideAnimationService'
];

export default triggerSliderAnimation;