import template from './content-slider-navigation.component.html';

import './content-slider-navigation.less';

let component = {
    template,
    require: {
        contentSliderController: '^contentSlider'
    }
};

export default component;