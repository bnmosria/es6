import FirstStepSlideModule from './first-step-slide/first-step-slide.module';
import SecondStepSlideModule from './second-step-slide/second-step-slide.module';
import ThirdStepSlideModule from './third-step-slide/third-step-slide.module';
import { SvgIconsModule } from '../svg-icons/svg-icons.module';

let StepsSlidesModule = angular
    .module('stepsSlides', [
        FirstStepSlideModule,
        SecondStepSlideModule,
        SvgIconsModule,
        ThirdStepSlideModule
    ])
    .name;

export default StepsSlidesModule;
