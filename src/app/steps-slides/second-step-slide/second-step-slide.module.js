import SecondStepSlideComponent from './second-step-slide.component';

import '../shared/step-slide.less';

let SecondStepSlideModule = angular
    .module('secondStepSlide', [])
    .component('secondStepSlide', SecondStepSlideComponent)
    .name;

export default SecondStepSlideModule;