import FirstStepSlideComponent from './first-step-slide.component';

import '../shared/step-slide.less';

let FirstStepSlideModule = angular
    .module('firstStepSlide', [])
    .component('firstStepSlide', FirstStepSlideComponent)
    .name;

export default FirstStepSlideModule;