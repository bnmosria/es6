import ThirdStepSlideComponent from './third-step-slide.component';

import '../shared/step-slide.less';

let ThirdStepSlideModule = angular
    .module('thirdStepSlide', [])
    .component('thirdStepSlide', ThirdStepSlideComponent)
    .name;

export default ThirdStepSlideModule;