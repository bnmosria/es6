import { SliderArrowIconComponent } from './slider-arrow-icon.component';

import '../shared/svg-scope.less';

export const SliderArrowIconModule = angular
    .module('sliderArrow', [])
    .component('sliderArrow', SliderArrowIconComponent)
    .name;
