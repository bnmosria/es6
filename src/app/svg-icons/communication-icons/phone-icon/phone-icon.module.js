import PhoneIconComponent from './phone-icon.component';

import '../../shared/svg-scope.less';

let phoneIconModule = angular
    .module('phoneIcon', [])
    .component('phoneIcon', PhoneIconComponent)
    .name;

export default phoneIconModule;