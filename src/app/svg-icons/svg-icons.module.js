import { CertifiedIconModule } from './document-icons/certified-icon/certified-icon.module';
import CircleOneIconModule from './circle-icons/circle-one-icon/circle-one-icon.module';
import CircleThreeIconModule from './circle-icons/circle-three-icon/circle-three-icon.module';
import CircleTwoIconModule from './circle-icons/circle-two-icon/circle-two-icon.module';
import ContractIconModule from './document-icons/contract-icon/contract-icon.module';
import PhoneIconModule from './communication-icons/phone-icon/phone-icon.module';
import { CaretIconModule } from "./caret-icon/caret-icon.module";

export const SvgIconsModule = angular
    .module('svgIcons', [
        CertifiedIconModule,
        CircleOneIconModule,
        CircleThreeIconModule,
        CircleTwoIconModule,
        ContractIconModule,
        CaretIconModule,
        PhoneIconModule
    ])
    .name;
