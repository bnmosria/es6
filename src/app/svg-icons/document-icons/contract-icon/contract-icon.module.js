import ContractIconComponent from './contract-icon.component';

import '../../shared/svg-scope.less';

let contractIconModule = angular
    .module('contractIcon', [])
    .component('contractIcon', ContractIconComponent)
    .name;

export default contractIconModule;