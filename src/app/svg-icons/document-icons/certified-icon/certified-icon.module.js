import { CertifiedIconComponent } from './certified-icon.component';

import '../../shared/svg-scope.less';

export const CertifiedIconModule = angular
    .module('certifiedIcon', [])
    .component('certifiedIcon', CertifiedIconComponent)
    .name;
