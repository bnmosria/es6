import { CaretIconComponent } from './caret-icon.component';

import '../shared/svg-scope.less';

export const CaretIconModule = angular
    .module('caretIcon', [])
    .component('caretIcon', CaretIconComponent)
    .name;
