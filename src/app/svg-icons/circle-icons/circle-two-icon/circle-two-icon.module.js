import CircleTwoIconComponent from './circle-two-icon.component';

import '../../shared/svg-scope.less';

let circleTwoIconModule = angular
    .module('circleTwoIcon', [])
    .component('circleTwoIcon', CircleTwoIconComponent)
    .name;

export default circleTwoIconModule;