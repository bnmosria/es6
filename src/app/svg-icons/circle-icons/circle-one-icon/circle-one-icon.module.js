import CircleOneIconComponent from './circle-one-icon.component';

import '../../shared/svg-scope.less';

let circleOneIconModule = angular
    .module('circleOneIcon', [])
    .component('circleOneIcon', CircleOneIconComponent)
    .name;

export default circleOneIconModule;