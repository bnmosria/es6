import CircleThreeIconComponent from './circle-three-icon.component';

import '../../shared/svg-scope.less';

let circleThreeIconModule = angular
    .module('circleThreeIcon', [])
    .component('circleThreeIcon', CircleThreeIconComponent)
    .name;

export default circleThreeIconModule;