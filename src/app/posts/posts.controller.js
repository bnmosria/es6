export class PostsController {

    constructor(postsService) {
        this.postsService = postsService;

        this.currentPage = 1;
        this.posts = [];
    }

    $onInit() {
        this.postsService.getPosts()
            .then(response => this.posts = response.data);
    }

    paginate(pager) {
        this.postsItems = this.posts.slice(pager.startIndex, pager.endIndex + 1);
    }
}

PostsController.$inject = ['postsService'];
