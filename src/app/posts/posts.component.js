import template from './posts.component.html';
import { PostsController as controller } from "./posts.controller";

import './posts.less';

export const PostsComponent = {
    template,
    controller,
};
