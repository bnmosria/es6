export class PostsService {
    constructor($http) {
        this.$http = $http;
    }

    getPosts() {
        return this.$http.get('https://jsonplaceholder.typicode.com/posts');
    }
}

PostsService.$inject = ['$http'];
