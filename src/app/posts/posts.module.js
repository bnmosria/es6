import { PostsComponent } from "./posts.component";
import { PostsService } from "./services/posts.service";
import { PaginatorModule } from "../paginator/paginator.module";

export const PostsModule = angular
    .module('posts', [
        PaginatorModule
    ])
    .component('posts', PostsComponent)
    .service('postsService', PostsService)
    .name;
