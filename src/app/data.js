export const data = [{"type": "group",
  "key": "outpatientBenefits",
  "label": "Ambulante Leistungen",
  "subLabel": null,
  "tooltip": null,
  "grade": 2.1,
  "items": [{
    "type": "group",
    "key": "medicalService",
    "label": "Arztleistungen",
    "subLabel": null,
    "tooltip": null,
    "grade": null,
    "items": [{
      "type": "leaf",
      "key": "medReimbursement",
      "label": "Ambulante Arztbehandlung",
      "subLabel": null,
      "tooltip": "In allen Tarifen werden normale ambulante Behandlungen von der Versicherung bezahlt. In manchen Tarifen gibt es aber Bedingungen f\u00fcr besondere Behandlungen. Dann bezahlt die Versicherung zum Beispiel nur einen Teil der Kosten, wenn man direkt zum Facharzt geht oder wenn der behandelnde Arzt sehr teuer ist.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "100%",
        "description": "100%, Facharzt ohne \u00dcberweisung 80%",
        "signal": "green"
      }
    }, {
      "type": "leaf",
      "key": "medDirectMedicalConsultation",
      "label": "Ohne \u00dcberweisung zum Facharzt",
      "subLabel": null,
      "tooltip": "Manche Versicherungen wollen, dass Sie bei Beschwerden zuerst zu Ihrem Hausarzt gehen. Die Versicherungen bezahlen nur dann die vollen Behandlungskosten, wenn Ihr Hausarzt Sie zu einem Facharzt \u00fcberwiesen hat. Ansonsten m\u00fcssen Sie einen Teil selbst bezahlen. Einige Fach\u00e4rzte sind von dieser Einschr\u00e4nkung meist ausgenommen \u2013 zum Beispiel Augen\u00e4rzte, Kinder\u00e4rzte oder Gyn\u00e4kologen.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "80%",
        "description": "80%",
        "signal": "red"
      }
    }, {
      "type": "leaf",
      "key": "medAboveMaximumRate",
      "label": "Arzthonorare \u00fcber Regelsatz",
      "subLabel": "(> 2,3-fach)",
      "tooltip": "Die Kosten f\u00fcr die Behandlung durch \u00c4rzte sind in einer Geb\u00fchrenordnung festgelegt. Bei privat versicherten Patienten k\u00f6nnen \u00c4rzte aber auch h\u00f6here Geb\u00fchren verlangen, als dort festgelegt sind. Die Versicherungen bestimmen, bis zu welcher H\u00f6he sie die Kosten erstatten. H\u00e4ufige Grenzen sind der Regelh\u00f6chstsatz (das 2,3-fache) oder der H\u00f6chstsatz (das 3,5-fache).",
      "grade": null,
      "item": {
        "type": "icon",
        "value": true,
        "description": "Bis zum 3,5-fachen Satz",
        "signal": "green"
      }
    }, {
      "type": "leaf",
      "key": "drugReimbursement",
      "label": "Arzneimittel",
      "subLabel": null,
      "tooltip": "Im Normalfall bezahlen die Versicherungen alle rezeptpflichtigen Medikamente. Das gilt auch f\u00fcr Verbandsmaterial, zum Beispiel Kompressen, Pflaster oder Gipsverb\u00e4nde.<br>Achtung: Manche Tarife bezahlen nur einen Teil der Kosten.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "100%",
        "description": "100% bei Erstbehandlung durch Hausarzt, sonst 80%",
        "signal": "green"
      }
    }]
  }, {
    "type": "group",
    "key": "healthAid",
    "label": "Heil- und Hilfsmittel",
    "subLabel": null,
    "tooltip": null,
    "grade": null,
    "items": [{
      "type": "leaf",
      "key": "healthaidVisualAidLimit",
      "label": "Sehhilfen",
      "subLabel": "(z.B. Brille, Kontaktlinsen)",
      "tooltip": "Sehhilfen sind zum Beispiel Brillen oder Kontaktlinsen. Sie werden nur dann von Versicherungen bezahlt, wenn sie vom Arzt verordnet wurden. Je nach Tarif ist die H\u00f6he der Erstattung unterschiedlich.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "Bis zu 100\u20ac alle 3 Jahre",
        "description": "100% bis 100\u20ac alle 3 Jahre",
        "signal": "red"
      }
    }, {
      "type": "leaf",
      "key": "healthaidGeneralRefund",
      "label": "Erstattung Hilfsmittel",
      "subLabel": "(z.B. H\u00f6rger\u00e4te, Rollst\u00fchle)",
      "tooltip": "Hilfsmittel sind zum Beispiel H\u00f6rger\u00e4te, Prothesen oder Rollst\u00fchle. Einige Versicherungen legen in einem Katalog fest, welche Hilfsmittel erstattet werden.<br>Achtung: Die meisten Tarife bezahlen Hilfsmittel nur teilweise. \u00dcblich sind H\u00f6chstsummen oder Selbstbeteiligungen.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "100%",
        "description": "100%, bei mehrmaligen Bezug pro Jahr Zusage ben\u00f6tigt",
        "signal": "green"
      }
    }, {
      "type": "leaf",
      "key": "remedyGeneralReimbursement",
      "label": "Erstattung Heilmittel",
      "subLabel": "(z.B. Massagen, Ergotherapie)",
      "tooltip": "Heilmittel sind zum Beispiel Massagen, Krankengymnastik oder W\u00e4rmebehandlungen. Auch Sprach- und Ergo-Therapie geh\u00f6ren dazu. Die Versicherung legt in einem Katalog fest, welche Ma\u00dfnahmen sie bezahlt.<br>Achtung: Die meisten Tarife bezahlen Heilmittel nur teilweise oder bis zu einer H\u00f6chstsumme.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "90%",
        "description": "90%",
        "signal": "yellow"
      }
    }, {
      "type": "leaf",
      "key": "healthaidOpenHealthAidCatalog",
      "label": "Offener Hilfsmittelkatalog",
      "subLabel": "(inkl. zuk\u00fcnftiger Weiterentwicklungen)",
      "tooltip": "Hilfsmittel sind zum Beispiel H\u00f6rger\u00e4te, Prothesen oder Rollst\u00fchle. Die Versicherungen legen in einem Katalog fest, welche Hilfsmittel sie erstatten. Nur wenn der Katalog \"offen\" ist, sind auch Hilfsmittel abgedeckt, die erst in der Zukunft auf den Markt kommen.<br>Achtung: Einige Versicherungen erstatten die Hilfsmittel nur teilweise oder bis zu einer H\u00f6chstsumme.",
      "grade": null,
      "item": {
        "type": "icon",
        "value": true,
        "description": "Erstattung s\u00e4mtlicher Hilfsmittel",
        "signal": "green"
      }
    }]
  }, {
    "type": "group",
    "key": "alternativeMedicine",
    "label": "Alternativmedizin",
    "subLabel": null,
    "tooltip": null,
    "grade": null,
    "items": [{
      "type": "leaf",
      "key": "amedTreatmentScopeMethod",
      "label": "Umfang alternativmedizinischer Methoden",
      "subLabel": null,
      "tooltip": "Alternativmedizinische Behandlungsmethoden sind zum Beispiel Hom\u00f6opathie, Osteopathie oder Akkupunktur. Versicherungen erstatten diese Methoden nur, wenn gewisse Bedingungen erf\u00fcllt werden. Die alternativen Methoden d\u00fcrfen nicht teurer, aber m\u00fcssen genauso wirksam sein wie schulmedizinische Behandlungen. Durchschnittliche Leistungen sind Verfahren aus dem Geb\u00fchrenverzeichnis der Heilpraktiker (Geb\u00fcH) oder einem eigenen Katalog der Versicherung. \u00dcberdurchschnittlichen Leistungen enthalten auch Methoden aus dem Hufeland-Verzeichnis, einem sehr umfassenden Katalog f\u00fcr alternative Heilmethoden.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "\u00dcberdurchschnittliche Leistungen",
        "description": "\u00dcberdurchschnittliche Erstattung, auch alle aus Hufeland-Verzeichnis",
        "signal": "green"
      }
    }]
  }, {
    "type": "group",
    "key": "inpatientPsychotherapy",
    "label": "Ambulante Psychotherapie",
    "subLabel": null,
    "tooltip": null,
    "grade": null,
    "items": [{
      "type": "leaf",
      "key": "psychotherapyReimbursement",
      "label": "Psychotherapie",
      "subLabel": null,
      "tooltip": "Bei der Psychotherapie werden seelische Probleme wie zum Beispiel Depressionen, Verhaltensst\u00f6rungen oder Zw\u00e4nge behandelt. Die Therapie kann von \u00c4rzten mit einer Zusatzausbildung durchgef\u00fchrt werden. In diesem Fall muss die Versicherung die Behandlung bezahlen. Die meisten Versicherungen bezahlen die Therapie aber auch, wenn sie von einem Psychotherapeuten durchgef\u00fchrt wird.<br>Achtung: Manche Tarife erstatten die Psychotherapie gar nicht, nur einige Sitzungen oder nur eine H\u00f6chstsumme.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "70% bis\nmax. 50 Sitzungen",
        "description": "70% f\u00fcr max. 50 Sitzungen pro Jahr",
        "signal": "red"
      }
    }]
  }]
}, {
  "type": "group",
  "key": "inpatientBenefits",
  "label": "Station\u00e4re Leistungen",
  "subLabel": null,
  "tooltip": null,
  "grade": 1,
  "items": [{
    "type": "leaf",
    "key": "treatmentAttendingDoctor",
    "label": "Behandelnder Arzt im Krankenhaus",
    "subLabel": null,
    "tooltip": "Die meisten Patienten werden im Krankenhaus vom Stationsarzt behandelt. Bei manchen Tarifen wird aber auch eine Behandlung durch den Chefarzt oder einen Spezialisten garantiert. Einige Versicherungen bezahlen die Kosten auch dann.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "Chefarzt",
      "description": "Chefarzt",
      "signal": "green"
    }
  }, {
    "type": "leaf",
    "key": "hospitalizationAccommodation",
    "label": "Zimmerart im Krankenhaus",
    "subLabel": null,
    "tooltip": "In der Regel werden Patienten im Krankenhaus in Mehrbett-Zimmern untergebracht.<br>Viele Tarife bieten aber auch die Unterbringung in 1-Bett- oder 2-Bett-Zimmern an.<br>CHECK24-Tipp: F\u00fcr ein 1-Bett-Zimmer zahlen Sie monatlich oft viel mehr. Deshalb ist es g\u00fcnstiger, einen Tarif mit 2-Bett-Zimmer zu w\u00e4hlen. Bei Bedarf kann im Krankenhaus ein Aufschlag f\u00fcr ein 1-Bett-Zimmer pro Nacht bezahlt werden.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "1-Bett-Zimmer",
      "description": "1-Bett-Zimmer",
      "signal": "green"
    }
  }, {
    "type": "leaf",
    "key": "treatmentAboveMaximumRate",
    "label": "Arzthonorare \u00fcber H\u00f6chstsatz",
    "subLabel": "(> 3,5-fach)",
    "tooltip": "Die Kosten f\u00fcr die Behandlung durch \u00c4rzte sind in einer Geb\u00fchrenordnung festgelegt. Bei privat versicherten Patienten k\u00f6nnen \u00c4rzte aber auch h\u00f6here Geb\u00fchren berechnen, als dort festgelegt sind. Chef\u00e4rzte und Spezialisten verlangen oft mehr als den H\u00f6chstsatz (das 3,5-fache). Einige Versicherungen bezahlen die Kosten auch dann.",
    "grade": null,
    "item": {
      "type": "icon",
      "value": true,
      "description": "Unbegrenzt",
      "signal": "green"
    }
  }, {
    "type": "leaf",
    "key": "cureFollowupTreatment",
    "label": "Anschlussheilbehandlung",
    "subLabel": null,
    "tooltip": "Eine Anschlussheilbehandlung ist eine Ma\u00dfnahme, die direkt im Anschluss an einen Krankenhausaufenthalt stattfindet. Das kann zum Beispiel eine Reha-Ma\u00dfnahme nach einer Operation sein. Die meisten Versicherungen zahlen diese Ma\u00dfnahme nur, wenn sie sie vorher genehmigt haben. Au\u00dferdem muss die Ma\u00dfnahme vom Arzt verordnet worden sein und sp\u00e4testens zwei Wochen nach dem Krankenhausaufenthalt beginnen.",
    "grade": null,
    "item": {
      "type": "icon",
      "value": true,
      "description": "",
      "signal": "green"
    }
  }]
}, {
  "type": "group",
  "key": "dentalBenefits",
  "label": "Zahnleistungen",
  "subLabel": null,
  "tooltip": null,
  "grade": 2.9,
  "items": [{
    "type": "leaf",
    "key": "dentalTreatment",
    "label": "Zahnbehandlung",
    "subLabel": null,
    "tooltip": "Zahnbehandlungen sollen daf\u00fcr sorgen, dass Z\u00e4hne gesund bleiben und nicht ersetzt werden m\u00fcssen. H\u00e4ufige Anwendungen sind F\u00fcllungen, Wurzelbehandlungen oder die professionelle Zahnreinigung.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "100%, unbegrenzte H\u00f6he im 1. Jahr",
      "description": "100% ohne Beschr\u00e4nkung",
      "signal": "green"
    }
  }, {
    "type": "leaf",
    "key": "dentalDentures",
    "label": "Implantate & Zahnersatz",
    "subLabel": null,
    "tooltip": "Wenn ein Zahn ausf\u00e4llt oder vom Zahnarzt entfernt werden muss, ist ein Zahnersatz notwendig. Dann werden oft Implantate, Br\u00fccken, Kronen oder Prothesen eingesetzt. Die meisten Tarife erstatten nur einen Teil der Kosten.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "80%",
      "description": "80%, mit Zahnstaffel, bis 600\u20ac im 1. Jahr",
      "signal": "yellow"
    }
  }, {
    "type": "leaf",
    "key": "dentalInlay",
    "label": "Inlays",
    "subLabel": null,
    "tooltip": "Inlays sind Zahnf\u00fcllungen aus Kunststoff, Keramik oder Gold. Sie werden im Labor gefertigt und anschlie\u00dfend vom Zahnarzt eingesetzt. Viele Tarife erstatten nur einen Teil der Kosten.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "80%",
      "description": "80%, mit Zahnstaffel, bis 600\u20ac im 1. Jahr",
      "signal": "yellow"
    }
  }, {
    "type": "leaf",
    "key": "dentalNoMaximumRefund",
    "label": "Unbegrenzte Zahnleistungen",
    "subLabel": null,
    "tooltip": "Die meisten Versicherungen bezahlen in den ersten Jahren nur einen Teil der Kosten. Danach erstatten einige Versicherungen die Kosten unbegrenzt.<br>Achtung: Manchmal ist die Leistung begrenzt. Dann \u00fcbernehmen die Versicherungen auch nach einigen Jahren nur einen Teil der Kosten.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "Nein, maximale Erstattung 4.000\u20ac",
      "description": "Dauerhaft begrenzt auf 4.000\u20ac",
      "signal": "yellow"
    }
  }, {
    "type": "leaf",
    "key": "dentalOrthodontia",
    "label": "Kieferorthop\u00e4die",
    "subLabel": null,
    "tooltip": "Die Kieferorthop\u00e4die k\u00fcmmert sich um schiefe Z\u00e4hne und andere Kieferprobleme. Einige Tarife erstatten nur einen Teil der Kosten oder legen einen H\u00f6chstbetrag fest. Manchmal wird die Behandlung auch nur bis zu einem bestimmten Alter bezahlt.",
    "grade": null,
    "item": {
      "type": "text",
      "comment": "80%, ohne Altersbeschr\u00e4nkung",
      "description": "80%, ohne Altersbeschr\u00e4nkung, bis 600\u20ac im 1. Jahr",
      "signal": "yellow"
    }
  }]
}, {
  "type": "group",
  "key": "general",
  "label": "tariffFeatures.group.label.general",
  "subLabel": null,
  "tooltip": null,
  "grade": null,
  "items": [{
    "type": "group",
    "key": "insuranceEvaluation",
    "label": "CHECK24-Bewertung des Versicherers",
    "subLabel": null,
    "tooltip": null,
    "grade": 1.1,
    "items": [{
      "type": "leaf",
      "key": "evaluationContributionStability",
      "label": "Beitragsstabilit\u00e4t im Alter",
      "subLabel": null,
      "tooltip": "CHECK24 untersucht, wie stabil die Beitr\u00e4ge der Versicherungen im Alter bleiben. Dabei schauen wir auf die finanziellen R\u00fcckstellungen der Versicherungen und auf die Entwicklung der Beitr\u00e4ge in den letzten Jahren.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "sehr gut",
        "description": "Beitragsr\u00fcckstellungen in etwa im Marktdurchschnitt, moderate Beitragserh\u00f6hungen",
        "signal": "green"
      }
    }, {
      "type": "leaf",
      "key": "evaluationOrganizationCertainty",
      "label": "Unternehmenssicherheit",
      "subLabel": null,
      "tooltip": "CHECK24 untersucht, wie gut die Versicherungen f\u00fcr die Zukunft vorbereitet sind. Dabei schauen wir auf verschiedene Merkmale, zum Beispiel den Marktanteil, Verwaltungskosten, Rendite und weitere Bilanzkennzahlen.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "sehr gut",
        "description": "Sehr hohes Mitgliederwachstum, hohe Rendite in der Kapitalanlage, geringe Verwaltungskosten",
        "signal": "green"
      }
    }, {
      "type": "leaf",
      "key": "evaluationCustomerFocus",
      "label": "Kundenorientierung",
      "subLabel": null,
      "tooltip": "CHECK24 untersucht, wie kundenfreundlich die Versicherungen sind. Dabei schauen wir vor allem auf die Anzahl an Beschwerden \u00fcber die Versicherung. Au\u00dferdem schauen wir uns das Service-Angebot in Apps und auf der Webseite an. Auch die Kundenorientierung am Telefon und bei E-Mails bestimmt die Zufriedenheit der Kunden.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "sehr gut",
        "description": "Rechnungs-App mit Barcode- und Fotofunktion & Vertrags-App (\u00dcbersicht und Verwaltung Daten), geringe Beschwerdequote",
        "signal": "green"
      }
    }]
  }, {
    "type": "group",
    "key": "reimbursement",
    "label": "R\u00fcckerstattung und Bonus",
    "subLabel": null,
    "tooltip": null,
    "grade": 2.8,
    "items": [{
      "type": "leaf",
      "key": "provisionContributionReimbursement",
      "label": "Garantierte Beitragsr\u00fcckerstattung",
      "subLabel": null,
      "tooltip": "Wenn Sie in einem Jahr keine Rechnung bei der Versicherung einreichen, erhalten Sie einen Teil Ihrer bezahlten Beitr\u00e4ge zur\u00fcck. Wenn die Versicherung eine R\u00fcckerstattung garantiert, erhalten Sie in jedem Fall Geld zur\u00fcck, egal wie gut das Jahr f\u00fcr die Versicherung war.",
      "grade": null,
      "item": {
        "type": "icon",
        "value": false,
        "description": "",
        "signal": "red"
      }
    }, {
      "type": "leaf",
      "key": "provisionContributionReimbursementAmount1y",
      "label": "Variable Beitragsr\u00fcckerstattung",
      "subLabel": null,
      "tooltip": "Wenn Sie in einem Jahr keine Rechnung bei der Versicherung einreichen, erhalten Sie einen Teil der Beitr\u00e4ge zur\u00fcck. Die variable R\u00fcckerstattung wird aber nur dann gezahlt, wenn die Gesch\u00e4ftszahlen der Versicherung gut sind.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "200\u20ac im 1. Jahr; max. 500\u20ac",
        "description": "Erfolgsabh\u00e4ngige Erstattung von 200\u20ac im 1. Jahr",
        "signal": "green"
      }
    }, {
      "type": "leaf",
      "key": "provisionRefundDespiteMedicalCheckup",
      "label": "Beitragsr\u00fcckerstattung trotz Vorsorgeuntersuchungen",
      "subLabel": null,
      "tooltip": "Bei einigen Tarifen werden Beitr\u00e4ge auch dann erstattet, wenn Sie zu Vorsorgeuntersuchungen gehen (zum Beispiel zur Krebsvorsorge oder Zahnprophylaxe) und daf\u00fcr eine Rechnung einreichen.",
      "grade": null,
      "item": {
        "type": "icon",
        "value": true,
        "description": "",
        "signal": "green"
      }
    }]
  }, {
    "type": "group",
    "key": "hospitalPerDiem",
    "label": "Krankentagegeld",
    "subLabel": null,
    "tooltip": null,
    "grade": 1,
    "items": [{
      "type": "leaf",
      "key": "pdhospitalPayoutAmount",
      "label": "H\u00f6he Krankentagegeld",
      "subLabel": null,
      "tooltip": "Eine Krankentagegeld-Versicherung wird gebraucht, wenn man l\u00e4ngere Zeit krank ist und nicht arbeiten kann. Das Krankentagegeld sollte die eigenen monatlichen Kosten abdecken k\u00f6nnen. Angestellte bekommen in den ersten 42 Tagen einer Krankheit noch Lohn vom Arbeitgeber. Daher ist f\u00fcr sie eine Absicherung ab dem 43. Krankheitstag zu empfehlen. Selbstst\u00e4ndige und Freiberufler haben keine gesetzliche Lohnfortzahlung. Daher m\u00fcssen sie selbst einsch\u00e4tzen, ab welchem Krankheitstag sie sich versichern, um den Ausfall des Einkommens auszugleichen.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "50\u20ac ab dem 43. Tag",
        "description": "50\u20ac ab dem 43. Tag",
        "signal": ""
      }
    }]
  }, {
    "type": "group",
    "key": "tariffDetails",
    "label": "Selbstbeteiligung",
    "subLabel": null,
    "tooltip": null,
    "grade": 3,
    "items": [{
      "type": "leaf",
      "key": "provisionCostsharingLimit",
      "label": "Selbstbeteiligung p.a.",
      "subLabel": null,
      "tooltip": "Bei Tarifen mit Selbstbeteiligung zahlen Sie die Arztrechnungen bis zur festgelegten H\u00f6he selbst. Erst Rechnungen, die diese H\u00f6he \u00fcberschreiten, bezahlt die Versicherung. Daf\u00fcr zahlen Sie geringere monatliche Beitr\u00e4ge.<br>CHECK24-Tipp: Angestellte sollten eine geringe Selbstbeteiligung w\u00e4hlen, weil der Arbeitgeber keinen Anteil an der Selbstbeteiligung zahlt.",
      "grade": null,
      "item": {
        "type": "text",
        "comment": "1.000\u20ac f\u00fcr Ambulant, Zahn",
        "description": "1.000\u20ac bei Ambulant und Zahn pro Jahr",
        "signal": ""
      }
    }]
  }, {
    "type": "group",
    "key": "tariffCondition",
    "label": "Tarifbedingungen",
    "subLabel": null,
    "tooltip": null,
    "grade": null,
    "items": [{
      "type": "leaf",
      "key": "insuranceCondition",
      "label": "Versicherungsbedingungen",
      "subLabel": null,
      "tooltip": "In den Versicherungsbedingungen stehen alle wichtigen Informationen zum Tarif. Die Versicherungsbedingungen k\u00f6nnen Sie hier herunterladen.",
      "grade": null,
      "item": {
        "type": "file",
        "fileUrl": "\/filestore\/tariff_terms\/e66adf79921c5df584dd80d0c434e49f_5a4e073409bd6_20180104_115132\/e66adf79921c5df584dd80d0c434e49fHanseMerkur_KVS3,PSV.pdf",
        "fileLabel": "Allg. Versicherungsbedingungen"
      }
    }]
  }]
}]
