export class PaginatorController {
    constructor(paginatorService) {
        this.paginatorService = paginatorService;
        this.pager = {};
    }

    $onInit() {
        this._paginate(this.currentPage);
    }

    onItemclicked(event, page) {
        this._paginate(page);
    }

    _paginate(page) {
        this._setPage(page);

        if (angular.isFunction(this.onPaginate)) {
            this.onPaginate({ pager: this.pager });
        }
    }

    _setPage(page) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        this.pager = this.paginatorService.getPager(this.itemsCount, page);
    }
}

PaginatorController.$inject = ['paginatorService'];
