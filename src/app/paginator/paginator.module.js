import { PaginatorComponent } from "./paginator.component";
import { PaginatorService } from "./service/paginator.service";

export const PaginatorModule = angular
    .module('paginator', [])
    .component('paginator', PaginatorComponent)
    .service('paginatorService', PaginatorService)
    .name;
