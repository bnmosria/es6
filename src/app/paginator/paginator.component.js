import template from './paginator.component.html';
import { PaginatorController as controller } from "./paginator.controller";

import './paginator.less';

export const PaginatorComponent = {
    template,
    controller,
    bindings: {
        currentPage: '<',
        itemsCount: '<',
        onPaginate: '&',
    }
};
