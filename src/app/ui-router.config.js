import { PostsController } from "./posts/posts.controller";

export const UiRouterConfig = ($stateProvider, $urlRouterProvider) => {
    $stateProvider.state('posts', {
        url: '/posts?page',
        controller: PostsController,
        params: {
            page: {
                value: '0',
                squash: true,
            }
        }
    });
};

UiRouterConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
