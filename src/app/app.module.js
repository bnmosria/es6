import '@uirouter/angularjs';

import { SliderCarouselModule } from "./slider-carousel/slider-carousel.module";
import { PostsModule } from "./posts/posts.module";

import { AppComponent } from './app.component';

import { CompileConfig } from "./compile.config";
import { UiRouterConfig } from "./ui-router.config";
import {CtaButtonModule} from "./cta-button/cta-button.module";

angular
    .module('app', [
        'ngAnimate',
        'ui.router',

        SliderCarouselModule,
        PostsModule,
        CtaButtonModule,
    ])
    .component('appRoot', AppComponent)
    .config(CompileConfig)
    .config(UiRouterConfig);
