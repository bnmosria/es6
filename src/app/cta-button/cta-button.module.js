import { CtaButtonComponent } from "./cta-button.component";

export const CtaButtonModule = angular
    .module('cta-button', [])
    .component('ctaButton', CtaButtonComponent)
    .name;
