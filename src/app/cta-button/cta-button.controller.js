const DEFAULT_THEME = 'cta-button--primary';

export class CtaButtonController {
    $onInit() {
        const themePrefix = 'cta-button--';
        this.theme = this.mode ? themePrefix + this.mode : DEFAULT_THEME;
    }
}
