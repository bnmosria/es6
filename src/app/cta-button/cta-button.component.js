import template from './cta-button.component.html';
import { CtaButtonController as controller } from "./cta-button.controller";

import './cta-button.less';

export const CtaButtonComponent = {
    template,
    controller,
    transclude: true,
    bindings: {
        mode: '<',
    }
};
