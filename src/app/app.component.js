import template from './app.component.html';
import controller from './app.controller';
import './app.less';

export const AppComponent = {
    template,
    controller
};
